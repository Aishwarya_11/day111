package sortingnaturalorder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class Main {
    
    public static void main(String[] args) {
		/*
		 * // Set up test data String n[] = { new String("John"), new String("Karl"),
		 * new String("Groucho"), new String("Oscar") };
		 * 
		 * // Create a List from an array List<String> l = Arrays.asList(n);
		 * 
		 * // Perform the sorting operation Collections.sort(l);
		 * 
		 * System.out.println("Sorting list of strings = " + l);
		 * 
		 * // Set up test data Integer int1[] = { new Integer(56), new Integer(78), new
		 * Integer(34), new Integer(10) };
		 * 
		 * // Create a List from an array List<Integer> l2 = Arrays.asList(int1);
		 * 
		 * // Perform the sorting operation Collections.sort(l2);
		 * 
		 * System.out.println("Sorting list of numbers = " + l2);
		 */   
    	
    List<Person> pList=new ArrayList<Person>();
    pList.add(new Person(UUID.randomUUID().toString(), "John", 39));
    pList.add(new Person(UUID.randomUUID().toString(), "Marry", 43));
    pList.add(new Person(UUID.randomUUID().toString(), "Public", 28));
    Comparator<Person> comparator=MyPersonComparator.personComparator();
    System.out.println("Before Sorting---->\n");
    for(Person p: pList)
    {
    	System.out.println(p);
    }
    Collections.sort(pList, comparator);
   // Collections.sort(pList);
    System.out.println("After Sorting---->\n");
    for(Person p: pList)
    {
    	System.out.println(p);
    }
    Collections.sort(pList, comparator);
    
    
    
    }
    
}

