package maphashmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class StudentDashBoard {
	
	private Map<Integer, Student> stMap;
	private static Integer i=0;
	
	{
		stMap=new HashMap<Integer, Student>();
	}
	public void createAndAddStudent(Student student)
	{
		stMap.put(++i, student);
	}
	
	public void displayStudents() throws StudentNotFoundExeption
	{
		Collection<Student> students=stMap.values();
		if(students.isEmpty())
		{
			throw new StudentNotFoundExeption("Database Is Empty Try adding students");
		}
		Iterator<Student> i=students.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
	}
	
	
	public Student getStudentById(Integer id) throws StudentNotFoundExeption
	{
		
			
			Student student=stMap.get(id);
			if(student==null)
			{
				throw new StudentNotFoundExeption("No Student Found With The Given ID: "+id);
			}
			
		return student;
	}
	
	public void removeStudentById(Integer id) throws StudentNotFoundExeption
	{
		Student student=stMap.get(id);
		if(student==null)
		{
			throw new StudentNotFoundExeption("No Student Found With The Given ID: "+id);
		}
		student=stMap.remove(id);
		System.out.println("Student Deleted: "+student);
		
	}
}




