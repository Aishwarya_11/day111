package sethashsetfinddup2;

import java.util.HashSet;
import java.util.Set;

public class SetTest {

	public static void main(String[] args) {
		String[] str=
			{
					"hello",
					"hi",
					"hello",
					"hello",
					"hi"
			};
		
		Set<String> actual=new HashSet<String>();
		Set<String> dumps=new HashSet<String>();
		for(int i=0;i<str.length;i++)
		{
			if(!actual.add(str[i]))
			{
				dumps.add(str[i]);
			}
		}
		System.out.println(actual);
		System.out.println(dumps);
	}

}
