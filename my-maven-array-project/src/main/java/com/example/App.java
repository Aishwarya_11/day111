package com.example;

import com.example.demo.Bird;
import com.example.demo.Flyer;
import com.example.demo.SuperMan;

public class App {
	private Flyer[] flyers;

	public static void main(String[] args) {
		App app = new App();
		app.flyers = new Flyer[10];
		app.flyers[0] = new Bird();
		app.flyers[1] = new SuperMan();
		app.flyers[0].display();
		app.flyers[1].display();
	}
}

