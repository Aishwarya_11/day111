package com.example.demo;

public interface Flyer {
	
	public String land();
	public String takeOff();
	public String fly();
	public void display();

}
