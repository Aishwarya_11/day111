package com.example.demo;

public class CricketCoach extends Coach{
	
	private CoachType coachType;

	public CricketCoach() {
		super();
	}

	public CricketCoach(String coachId,String coachName,CoachType coachType) {
		super(coachId,coachName);
		this.coachType = coachType;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return super.getDailyWorkout()+"\nPractice Fielding Today.";
	}

	@Override
	public String getCoachDetails() {
		// TODO Auto-generated method stub
		return super.getCoachDetails()+"Coach Type: "+coachType.getCoachType();
	}
	
	
	
	

}
