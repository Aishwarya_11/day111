package com.example.demo;

public enum CoachType {

	T20("This Coah Is T20 Coach"), 
	ONEDAY("This Coach Is One Day Cricket Coach"),
	TEST("This Coach Is Test Cricket Coach");

	private String coachType;

	private CoachType(String coachType) {
		this.coachType = coachType;
	}

	public String getCoachType() {
		return coachType;
	}

}


