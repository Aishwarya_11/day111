package com.example.demo;

public class Coach {

	private String coachId;
	private String coachName;

	public Coach() {
		super();
	}

	public Coach(String coachId, String coachName) {
		super();
		this.coachId = coachId;
		this.coachName = coachName;
	}

	public String getDailyWorkout() {
		return "Suggesting workout....";
	}
	
	public String getCoachDetails()
	{
		return String.format("Coach ID: %s Coach Name: %s",coachId,coachName );
	}

}

